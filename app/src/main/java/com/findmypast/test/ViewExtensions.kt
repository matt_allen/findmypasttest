package com.findmypast.test

import android.view.View

fun <T> Any.cast(): T = (this as T)

fun View.setVisible(visible: Boolean) {
    visibility = if (visible) View.VISIBLE else View.GONE
}
