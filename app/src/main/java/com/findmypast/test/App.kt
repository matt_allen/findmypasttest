package com.findmypast.test

import android.app.Application
import com.findmypast.test.api.apiModule
import com.findmypast.test.data.dataModule
import com.findmypast.test.ui.uiModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App: Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@App)
            modules(uiModule, apiModule, dataModule)
        }
    }
}
