package com.findmypast.test.api

import kotlinx.serialization.Serializable
import java.time.LocalDate
import java.time.format.DateTimeFormatter

@Serializable
sealed class PersonData {
    abstract val id: String
    abstract val firstname: String
    abstract val surname: String
    abstract val dob: String
    abstract val image: String?

    fun getDateOfBirth(): LocalDate? {
        if (dob.isBlank()) return null
        return LocalDate.parse(dob, DateTimeFormatter.ofPattern(DOB_FORMAT))
    }

    companion object {
        private const val DOB_FORMAT = "ddMMyyyy"
    }
}

@Serializable
data class PersonResponse(
    override val id: String,
    override val firstname: String,
    override val surname: String,
    override val dob: String,
    override val image: String?,
): PersonData()

@Serializable
data class PersonRelationsResponse(
    override val id: String,
    override val firstname: String,
    override val surname: String,
    override val dob: String,
    override val image: String?,
    val relationships: Relationships,
): PersonData()

@Serializable
data class Relationships(
    val spouse: String? = null,
    val mother: String? = null,
    val father: String? = null,
    val children: List<String> = emptyList(),
)
