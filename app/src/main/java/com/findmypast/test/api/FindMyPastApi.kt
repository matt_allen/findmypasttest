package com.findmypast.test.api

import retrofit2.http.GET
import retrofit2.http.Path

interface FindMyPastApi {
    @GET("profiles/{userId}")
    suspend fun getPeopleForUser(@Path("userId") name: String): ApiDataWrapper<List<PersonResponse>>

    @GET("profile/{personId}/{userId}")
    suspend fun getPersonRelations(@Path("personId") personId: String, @Path("userId") name: String): ApiDataWrapper<PersonRelationsResponse>
}
