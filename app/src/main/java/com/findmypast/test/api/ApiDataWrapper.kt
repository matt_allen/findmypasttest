package com.findmypast.test.api

import kotlinx.serialization.Serializable

@Serializable
data class ApiDataWrapper<T>(val success: Boolean, val data: T)
