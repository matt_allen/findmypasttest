package com.findmypast.test.ui

import com.findmypast.test.ui.people.PeopleViewModel
import com.findmypast.test.ui.tree.TreeViewModel
import com.findmypast.test.ui.user.UserIdViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val uiModule = module {
    viewModel { UserIdViewModel(get()) }
    viewModel { PeopleViewModel(get(), get()) }
    viewModel { (personId: String) -> TreeViewModel(personId, get(), get()) }
}
