package com.findmypast.test.ui.people

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.findmypast.test.api.PersonResponse
import com.findmypast.test.api.Response
import com.findmypast.test.cast
import com.findmypast.test.databinding.FragmentPeopleBinding
import com.findmypast.test.setVisible
import com.findmypast.test.ui.MainActivity
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.text.DateFormat
import java.time.format.DateTimeFormatter

class PeopleFragment: Fragment() {
    private var binding: FragmentPeopleBinding? = null
    private val viewModel by viewModel<PeopleViewModel>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return FragmentPeopleBinding.inflate(inflater).also { binding = it }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.list?.adapter = PeopleListAdapter(
            { activity?.cast<MainActivity>()?.showTree(it.id) },
            DateTimeFormatter.ofPattern("dd/MM/yyyy")
        )
        viewModel.people.observe(viewLifecycleOwner) { setResponse(it) }
    }

    private fun setResponse(response: Response<List<PersonResponse>>) {
        if (response.state == Response.State.SUCCESS) {
            binding?.list
                ?.adapter
                ?.cast<PeopleListAdapter>()
                ?.setItems(response.data ?: emptyList())
        }
        binding?.list?.setVisible(response.state == Response.State.SUCCESS)
        binding?.error?.setVisible(response.state == Response.State.FAILURE)
        binding?.loading?.setVisible(response.state == Response.State.LOADING)
    }
}
