package com.findmypast.test.ui.user

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.findmypast.test.R
import com.findmypast.test.data.UserRepository
import com.findmypast.test.ui.LiveDataEvent
import com.findmypast.test.ui.asEvent

class UserIdViewModel(private val repo: UserRepository): ViewModel() {
    private val _error = MutableLiveData(LiveDataEvent(0, true))

    val isSignedIn = repo.isSignedIn.asLiveData()
    val error: LiveData<LiveDataEvent<Int>> = _error

    fun signIn(username: String) {
        if (username.isNotBlank()) {
            _error.postValue(LiveDataEvent(0, true))
            repo.signIn(username)
        }
        else {
            _error.postValue(R.string.username_invalid.asEvent())
        }
    }
}
