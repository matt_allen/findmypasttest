package com.findmypast.test.ui.people

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.findmypast.test.api.PersonResponse
import com.findmypast.test.databinding.ItemPersonListBinding
import java.text.DateFormat
import java.time.format.DateTimeFormatter

class PeopleListAdapter(
    private val onClick: (PersonResponse) -> Unit,
    private val dateFormat: DateTimeFormatter,
): RecyclerView.Adapter<PeopleListAdapter.PeopleViewHolder>() {
    private var items = emptyList<PersonResponse>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PeopleViewHolder {
        return PeopleViewHolder(ItemPersonListBinding.inflate(LayoutInflater.from(parent.context)))
    }

    override fun onBindViewHolder(holder: PeopleViewHolder, position: Int) {
        holder.bind(items[position], onClick, dateFormat)
    }

    override fun getItemCount(): Int = items.size

    fun setItems(items: List<PersonResponse>) {
        this.items = items
        notifyDataSetChanged()
    }

    inner class PeopleViewHolder(private val binding: ItemPersonListBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(person: PersonResponse, onClick: (PersonResponse) -> Unit, dateFormat: DateTimeFormatter) {
            binding.name.text = "${person.firstname} ${person.surname}"
            binding.root.setOnClickListener { onClick.invoke(person) }
            val dob = person.getDateOfBirth()
            if (dob != null) {
                binding.dob.text = dateFormat.format(dob)
            }
            Glide.with(binding.root)
                .load(person.image)
                .circleCrop()
                .into(binding.image)
        }
    }
}
