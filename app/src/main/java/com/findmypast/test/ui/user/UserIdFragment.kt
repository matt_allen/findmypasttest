package com.findmypast.test.ui.user

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.findmypast.test.cast
import com.findmypast.test.databinding.FragmentUsernameBinding
import com.findmypast.test.ui.MainActivity
import com.google.android.material.snackbar.Snackbar
import org.koin.androidx.viewmodel.ext.android.viewModel

class UserIdFragment: Fragment() {
    private var binding: FragmentUsernameBinding? = null
    private val viewModel by viewModel<UserIdViewModel>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return FragmentUsernameBinding.inflate(inflater).also { binding = it }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.isSignedIn.observe(viewLifecycleOwner) { if (it) onUserSignedIn() }
        viewModel.error.observe(viewLifecycleOwner) {
            it.getContentIfNotHandled()?.apply { Snackbar.make(view, this, Snackbar.LENGTH_SHORT).show() }
        }
        binding?.submit?.setOnClickListener { viewModel.signIn(binding?.username?.text?.toString() ?: "") }
    }

    private fun onUserSignedIn() {
        requireActivity().cast<MainActivity>().goToPeopleList()
    }
}
