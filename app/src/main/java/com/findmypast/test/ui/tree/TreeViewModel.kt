package com.findmypast.test.ui.tree

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.map
import com.findmypast.test.data.PersonRepository
import com.findmypast.test.data.UserRepository
import com.findmypast.test.tryOrNull
import kotlinx.coroutines.flow.map

class TreeViewModel(
    personId: String,
    personRepo: PersonRepository,
    userRepo: UserRepository,
): ViewModel() {
    val me = userRepo.username
        .map { personRepo.fetchRelations(it ?: "", personId) }
        .asLiveData()

    private val relations = me.map { it.data.relationships }
    val father = relations.map { tryOrNull { personRepo.getPerson(it.father) } }
    val mother = relations.map { tryOrNull { personRepo.getPerson(it.mother) } }
    val spouse = relations.map { tryOrNull { personRepo.getPerson(it.spouse) } }
    val children = relations.map { it.children.map { id -> tryOrNull { personRepo.getPerson(id) } } }
}
