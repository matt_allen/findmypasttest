package com.findmypast.test.ui.people

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.findmypast.test.data.PersonRepository
import com.findmypast.test.data.UserRepository
import kotlinx.coroutines.flow.flatMapMerge

class PeopleViewModel(userRepo: UserRepository, personRepo: PersonRepository): ViewModel() {
    val people = userRepo.username
        .flatMapMerge { personRepo.fetchPeople(it ?: "") }
        .asLiveData()
}
