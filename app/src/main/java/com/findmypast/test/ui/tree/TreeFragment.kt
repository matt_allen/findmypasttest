package com.findmypast.test.ui.tree

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.LinearLayout
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.findmypast.test.R
import com.findmypast.test.api.PersonData
import com.findmypast.test.api.PersonResponse
import com.findmypast.test.databinding.FragmentTreeBinding
import com.findmypast.test.databinding.ItemPersonTreeBinding
import com.findmypast.test.setVisible
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import java.time.format.DateTimeFormatter

class TreeFragment: Fragment() {
    private var binding: FragmentTreeBinding? = null
    private val viewModel by viewModel<TreeViewModel> { parametersOf(requireArguments()[EXTRA_PERSON_ID]) }
    private val formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy")

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return FragmentTreeBinding.inflate(inflater).also { binding = it }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.me.observe(viewLifecycleOwner) { applyPersonToView(it.data, binding?.me) }
        viewModel.father.observe(viewLifecycleOwner) { applyPersonToView(it, binding?.father) }
        viewModel.mother.observe(viewLifecycleOwner) { applyPersonToView(it, binding?.mother) }
        viewModel.spouse.observe(viewLifecycleOwner) { applyPersonToView(it, binding?.spouse) }
        viewModel.children.observe(viewLifecycleOwner) { showChildren(it.filterNotNull()) }
    }

    private fun applyPersonToView(person: PersonData?, view: ItemPersonTreeBinding?) {
        if (person == null) {
            view?.firstname?.text = getString(R.string.missing)
        }
        else {
            view?.firstname?.text = person.firstname
            view?.surname?.text = person.surname
            person.getDateOfBirth()?.also {
                view?.dob?.text = formatter.format(it)
            }
            view?.image?.apply {
                Glide.with(this@TreeFragment)
                    .load(person.image)
                    .circleCrop()
                    .into(this)
            }
        }
    }

    private fun showChildren(children: List<PersonResponse>) {
        if (children.isEmpty()) {
            binding?.childConnectLine?.setVisible(false)
            binding?.childLineHorizontal?.setVisible(false)
        }
        else {
            val inflater = LayoutInflater.from(requireContext())
            repeat(children.size) {
                val view = ItemPersonTreeBinding.inflate(inflater)
                children[it].getDateOfBirth()?.apply { view.dob.text = formatter.format(this) }
                view.firstname.text = children[it].firstname
                view.surname.text = children[it].surname
                children[it].image?.apply {
                    Glide.with(this@TreeFragment)
                        .load(this)
                        .circleCrop()
                        .into(view.image)
                }
                val margin = resources.getDimension(R.dimen.margin)
                val params = LinearLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT)
                params.setMargins(if (it > 0) margin.toInt() else 0, 0, if (it < children.size-1) margin.toInt() else 0, 0)
                this.binding?.childrenContainer?.addView(view.root, params)
            }
        }
    }

    companion object {
        private const val EXTRA_PERSON_ID = "person_id"

        fun newInstance(personId: String): TreeFragment {
            val bundle = bundleOf(EXTRA_PERSON_ID to personId)
            return TreeFragment().apply { arguments = bundle }
        }
    }
}
