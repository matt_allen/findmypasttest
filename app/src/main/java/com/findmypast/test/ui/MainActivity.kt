package com.findmypast.test.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.findmypast.test.R
import com.findmypast.test.databinding.ActivityMainBinding
import com.findmypast.test.ui.people.PeopleFragment
import com.findmypast.test.ui.tree.TreeFragment
import com.findmypast.test.ui.user.UserIdFragment

class MainActivity : AppCompatActivity() {
    private var binding: ActivityMainBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding?.root)
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragment, UserIdFragment())
            .commit()
    }

    fun goToPeopleList() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragment, PeopleFragment())
            .commit()
    }

    fun showTree(personId: String) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragment, TreeFragment.newInstance(personId))
            .addToBackStack("")
            .commit()
    }
}
