package com.findmypast.test.ui

class LiveDataEvent<out T>(private val content: T, private var handled: Boolean = false) {
    override fun equals(other: Any?): Boolean {
        val a = other as? LiveDataEvent<T>
        return a != null && a.peekContent() == content
    }

    override fun hashCode(): Int {
        var result = content?.hashCode() ?: 0
        result = 31 * result + handled.hashCode()
        return result
    }

    fun setHandled(handled: Boolean) {
        this.handled = handled
    }

    fun getContentIfNotHandled(): T? = content
        .takeUnless { handled }
        ?.let { content }
        ?.also { handled = true }

    fun peekContent(): T = content
    fun isHandled(): Boolean = handled
}

fun <T> T.asEvent(handled: Boolean = false): LiveDataEvent<T> = LiveDataEvent(this, handled)
