package com.findmypast.test.data

import com.findmypast.test.persistence.SharedPrefsDataSource
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.map

class UserRepository(private val prefs: SharedPrefsDataSource) {
    private val _username = MutableStateFlow(prefs.getString(KEY_USERNAME))
    val username: StateFlow<String?> = _username
    val isSignedIn = username.map { !it.isNullOrBlank() }

    fun signIn(username: String) {
        prefs.setString(KEY_USERNAME, username)
        _username.tryEmit(username)
    }

    fun signOut() {
        prefs.remove(KEY_USERNAME)
        _username.tryEmit(null)
    }

    companion object {
        private const val KEY_USERNAME = "username"
    }
}
