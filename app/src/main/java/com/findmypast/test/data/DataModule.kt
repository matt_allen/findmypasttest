package com.findmypast.test.data

import android.content.Context
import com.findmypast.test.persistence.SharedPrefsDataSource
import kotlinx.coroutines.Dispatchers
import org.koin.dsl.module

val dataModule = module {
    factory { SharedPrefsDataSource(get<Context>().getSharedPreferences("preferences", Context.MODE_PRIVATE)) }
    single { UserRepository(get()) }
    single { PersonRepository(get(), Dispatchers.IO) }
}
