package com.findmypast.test.data

import com.findmypast.test.api.FindMyPastApi
import com.findmypast.test.api.PersonResponse
import com.findmypast.test.api.Response
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

class PersonRepository(private val api: FindMyPastApi, private val dispatcher: CoroutineDispatcher) {
    private val peopleCache = mutableSetOf<PersonResponse>()

    fun fetchPeople(username: String) = flow<Response<List<PersonResponse>>> {
        emit(Response.loading())
        api.runCatching { api.getPeopleForUser(username) }
            .onFailure { emit(Response.error(it.cause)) }
            .onSuccess {
                peopleCache.addAll(it.data)
                emit(Response.success(it.data))
            }
    }.flowOn(dispatcher)

    suspend fun fetchRelations(username: String, personId: String) = api.getPersonRelations(personId, username)

    fun getPerson(personId: String?) = peopleCache.first { it.id == personId }
}
