package com.findmypast.test.persistence

import android.content.SharedPreferences

/**
 * Abstraction of SharedPreference to allow for easy unit testing.
 */
class SharedPrefsDataSource(private val sharedPrefs: SharedPreferences) {
    fun setString(key: String, value: String) {
        sharedPrefs.edit().putString(key, value).apply()
    }

    fun getString(key: String): String? = sharedPrefs.getString(key, null)
    fun remove(key: String) = sharedPrefs.edit().remove(key).apply()
}
