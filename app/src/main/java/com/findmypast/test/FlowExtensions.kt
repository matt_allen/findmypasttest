package com.findmypast.test

fun <T> tryOrNull(f: () -> T): T? {
    return try {
        f.invoke()
    }
    catch (e: Exception) {
        null
    }
}
