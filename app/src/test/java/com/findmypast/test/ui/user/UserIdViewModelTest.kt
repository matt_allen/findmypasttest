package com.findmypast.test.ui.user

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.findmypast.test.data.UserRepository
import com.findmypast.test.observeFromTest
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import junit.framework.TestCase.assertFalse
import kotlinx.coroutines.flow.flowOf
import org.junit.Rule
import org.junit.Test

class UserIdViewModelTest {
    @get:Rule val instantExecutorRule = InstantTaskExecutorRule()

    @Test
    fun testUserSignIn_whenUsernameIsBlank_givenNoUserAlready_thenPresentError() {
        val repo = mock<UserRepository> {
            on { isSignedIn } doReturn flowOf(false)
        }
        val vm = UserIdViewModel(repo)
        val errors = vm.error.observeFromTest()
        vm.signIn("")
        assertFalse(errors.history().last().isHandled())
    }

    @Test
    fun testUserSignIn_whenUsernameIsNotBlank_givenNoUserAlready_thenSignIn() {
        val repo = mock<UserRepository> {
            on { isSignedIn } doReturn flowOf(false)
        }
        val vm = UserIdViewModel(repo)
        vm.signIn("valid")
        verify(repo, times(1)).signIn(any())
    }
}
