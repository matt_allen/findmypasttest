package com.findmypast.test.api

import junit.framework.TestCase.assertEquals
import org.junit.Test
import java.time.Month

class PersonResponseTest {
    @Test
    fun testDateOfBirthFormat() {
        val person = PersonResponse("", "", "", "04051987", "")
        assertEquals(4, person.getDateOfBirth().dayOfMonth)
        assertEquals(Month.MAY, person.getDateOfBirth().month)
        assertEquals(1987, person.getDateOfBirth().year)
    }
}
