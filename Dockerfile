FROM node:latest
RUN git clone https://github.com/findmypast/fmp-mobile-code-test
WORKDIR fmp-mobile-code-test
RUN npm i
CMD npm start